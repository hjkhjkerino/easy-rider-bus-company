# Write your awesome code here
import json
import re


def check_req(entry, key):
    if key != 'stop_type':
        if entry[key] == '':
            return False
        else:
            return True
    else:
        return True


def check_type(entry, key):
    if key == 'bus_id':
        if type(entry[key]) is int:
            return True
        else:
            return False
    elif key == 'stop_id':
        if type(entry[key]) is int:
            return True
        else:
            return False
    elif key == 'stop_name':
        suffix = ['Road', 'Avenue', 'Boulevard', 'Street', 'St.', 'Av.']
        if not type(entry[key]) is str:
            return False
        split_stop_name = entry[key].split()
        if len(split_stop_name) < 1:
            return False
        else:
            return True
            # if (split_stop_name[0] != split_stop_name[0].capitalize()) or (split_stop_name[-1] not in suffix):
            #     return False
            # else:
            #     return True
    elif key == 'next_stop':
        if type(entry[key]) is int:
            return True
        else:
            return False
    elif key == 'stop_type':
        if entry[key] == '':
            return True
        else:
            if type(entry[key]) is str:
                if entry[key] in 'SOF':
                    return True
                else:
                    return False
            else:
                return False
    else:
        if not type(entry[key]) is str:
            return False
        time_str = entry[key].split(':')
        if len(time_str) < 2:
            return False
        else:
            time_str = entry[key].split(':')
            if (int(time_str[0]) > 24) or (int(time_str[0]) < 0):
                return False
            elif (int(time_str[1]) > 60) or (int(time_str[1]) < 0):
                return False
            else:
                return True


def check_is_format(key):
    if key == 'stop_name' or key == 'stop_type' or key == 'a_time':
        return True
    else:
        return False


def check_format_stop_name(entry, key):
    template = r'([A-Z]\w*\s)+(Road|Avenue|Boulevard|Street)$'
    if re.match(template, entry[key]):
        return True
    else:
        return False


def check_format_stop_type(entry, key):
    if entry[key] == '':
        return True
    template = r'(S|O|F)$'
    if re.match(template, entry[key]):
        return True
    else:
        return False


def check_format_a_time(entry, key):
    template = r'[0-2][0-9]:[0-6][0-9]$'
    if re.match(template, entry[key]):
        return True
    else:
        return False


def check_stop(bus_id, bus_stop, bus_id_dict):
    if bus_id not in bus_id_dict:
        bus_id_dict[bus_id] = {}
    else:
        if bus_stop not in bus_id_dict[bus_id]:
            if len(bus_id_dict[bus_id]) == 0:
                bus_id_dict[bus_id] = {bus_stop}
            else:
                bus_id_dict[bus_id].add(bus_stop)


def is_earlier(prev_time, cur_time):
    split_prev = prev_time.split(':')
    split_cur = cur_time.split(':')
    if int(split_prev[0]) > int(split_cur[0]):
        return True
    elif int(split_prev[0]) == int(split_cur[0]) and int(split_prev[1]) > int(split_cur[1]):
        return True
    else:
        return False


# 1: Prospekt Avenue, 2: Pilotow Street, 3: Elm Street, 4: Bourbon Street, 5: Fifth Avenue, 6: Sunset Boulevard, 7: Sesame Street
class BusLine:
    stops = set()
    transfers = set()

    def __init__(self, bus_id):
        self.bus_id = bus_id
        self.start = set()
        self.finish = set()

    def add_stop(self, stop_name):
        if stop_name in self.stops:
            self.transfers.add(stop_name)
        else:
            self.stops.add(stop_name)

    def add_start(self, stop_name):
        self.start.add(stop_name)

    def add_finish(self, stop_name):
        self.finish.add(stop_name)


in_str = input()
in_str.replace('[', '')
in_str.replace(']', '')
in_dict = json.loads(in_str)
bus_dict = {}
cur_id = ''
cur_time = ''
prev_time = ''
time_err = False
cur_err = False
print('Arrival time test:')
for entry in in_dict:
    bus_id = entry['bus_id']
    if bus_id != cur_id:
        cur_err = False
        cur_id = bus_id
        cur_time = entry['a_time']
        prev_time = entry['a_time']
    if cur_err:
        continue
    prev_time = cur_time
    cur_time = entry['a_time']
    if is_earlier(prev_time, cur_time):
        print(f'bus_id line {bus_id}: wrong time on station {entry["stop_name"]}')
        cur_err = True
        time_err = True
        continue
if not time_err:
    print('OK')
# error = False
# for entry in in_dict:
#     if str(entry['bus_id']) not in bus_dict:
#         bus_dict[str(entry['bus_id'])] = BusLine(entry['bus_id'])
#     bus_dict[str(entry['bus_id'])].add_stop(entry['stop_name'])
#     if entry['stop_type'] == 'S':
#         bus_dict[str(entry['bus_id'])].add_start(entry['stop_name'])
#     elif entry['stop_type'] == 'F':
#         bus_dict[str(entry['bus_id'])].add_finish(entry['stop_name'])
# for entry in bus_dict.values():
#     if len(entry.start) > 1:
#         error = True
#         print(f'Too many starts on line: {entry.bus_id}')
#         break
#     elif len(entry.finish) > 1:
#         error = True
#         print(f'Too many finishes on line: {entry.bus_id}')
#         break
#     elif len(entry.start) < 1 or len(entry.finish) < 1:
#         error = True
#         print(f'There is no start or end stop for the line: {entry.bus_id}')
#         break
# if not error:
#     starts = set()
#     stops = set()
#     for entry in bus_dict.values():
#         starts.update(entry.start)
#         stops.update(entry.finish)
#     print(f'Start stops: {len(starts)} {sorted(starts)}')
#     print(f'Transfer stops: {len(BusLine.transfers)} {sorted(BusLine.transfers)}')
#     print(f'Finish stops: {len(stops)} {sorted(stops)}')



# for entry in in_dict:
#     cur_bus_id = entry['bus_id']
#     check_stop(entry['bus_id'], entry['stop_id'], bus_dict)
#     check_stop(entry['bus_id'], entry['next_stop'], bus_dict)
# print('Line names and number of stops:')
# for entry in bus_dict:
#     print(f'bus_id: {entry}, stops: {len(bus_dict[entry])}')


# for entry in in_dict:
#     for key in entry:
#         if check_is_format(key):
#             if key == 'stop_name':
#                 if not check_format_stop_name(entry, key):
#                     total_err_format += 1
#                     err_dict[key] = err_dict[key] + 1
#             elif key == 'stop_type':
#                 if not check_format_stop_type(entry, key):
#                     total_err_format += 1
#                     err_dict[key] = err_dict[key] + 1
#             elif key == 'a_time':
#                 if not check_format_a_time(entry,key):
#                     total_err_format += 1
#                     err_dict[key] = err_dict[key] + 1
# print(f'Format validation: {total_err_format} errors')
# for entry in err_dict:
#     print(f'{entry}: {err_dict[entry]}')
# err_dict = {'bus_id': 0, 'stop_id': 0, 'stop_name': 0, 'next_stop': 0, 'stop_type': 0, 'a_time': 0}
# total_err = 0
# for entry in in_dict:
#     for key in entry:
#         if not check_req(entry, key):
#             total_err += 1
#             err_dict[key] = err_dict[key] + 1
#         elif not check_type(entry, key):
#             total_err += 1
#             err_dict[key] = err_dict[key] + 1
# print(f'Type and required field validation: {total_err} errors')
# for entry in err_dict:
#     print(f'{entry}: {err_dict[entry]}')
